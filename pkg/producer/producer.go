package producer

type Producer interface {
	Produce() error
}
