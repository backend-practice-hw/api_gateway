package config

import (
	"log"
	"os"

	"github.com/joho/godotenv"
	"github.com/spf13/cast"
)

type Config struct {
	ServiceName string
	Environment string
	LoggerLevel string

	HTTPPort string

	CatalogGrpcServiceHost string
	CatalogGrpcServicePort string

	UserGrpcServiceHost string
	UserGrpcServicePort string

	OrderGrpcServiceHost string
	OrderGrpcServicePort string

	KafkaHost string
	KafkaPort string
}

func Load() Config {
	if err := godotenv.Load(); err != nil {
		log.Println("error is while loading .env file")
	}

	cfg := Config{}

	cfg.ServiceName = cast.ToString(getOrReturnDefault("SERVICE_NAME", "store"))
	cfg.Environment = cast.ToString(getOrReturnDefault("ENVIRONMENT", "dev"))

	cfg.HTTPPort = cast.ToString(getOrReturnDefault("HTTP_PORT", ":8080"))

	cfg.CatalogGrpcServiceHost = cast.ToString(getOrReturnDefault("CATALOG_GRPC_SERVICE_HOST", "localhost"))
	cfg.CatalogGrpcServicePort = cast.ToString(getOrReturnDefault("CATALOG_GRPC_SERVICE_PORT", ":8080"))

	cfg.UserGrpcServiceHost = cast.ToString(getOrReturnDefault("USER_GRPC_SERVICE_HOST", "localhost"))
	cfg.UserGrpcServicePort = cast.ToString(getOrReturnDefault("USER_GRPC_SERVICE_PORT", ":8080"))

	cfg.OrderGrpcServiceHost = cast.ToString(getOrReturnDefault("ORDER_GRPC_SERVICE_HOST", "localhost"))
	cfg.OrderGrpcServicePort = cast.ToString(getOrReturnDefault("ORDER_GRPC_SERVICE_PORT", ":8080"))

	cfg.KafkaHost = cast.ToString(getOrReturnDefault("KAFKA_HOST", "localhost"))
	cfg.KafkaPort = cast.ToString(getOrReturnDefault("KAFKA_PORT", ":8080"))

	return cfg
}

func getOrReturnDefault(key string, defaultValue interface{}) interface{} {
	value := os.Getenv(key)
	if value != "" {
		return value
	}

	return defaultValue
}
