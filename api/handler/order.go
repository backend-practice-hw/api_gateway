package handler

import (
	pbO "api_gateway/genproto/order_service"
	"api_gateway/pkg/logger"
	"context"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

// CreateOrder godoc
// @Router       /order [POST]
// @Summary      Create a new order
// @Description  Create a new order
// @Tags         order
// @Accept       json
// @Produce      json
// @Param        order  body models.CreateOrder false "order"
// @Success      201  {object}  models.Order
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateOrder(c *gin.Context) {
	request := pbO.Order{}
	if err := c.ShouldBindJSON(&request); err != nil {
		h.log.Error("error is while reading body", logger.Error(err))
		return
	}
	resp, err := h.services.OrderService().Create(context.Background(), &request)
	if err != nil {
		h.log.Error("error is while creating  order", logger.Error(err))
		return
	}
	handleResponse(c, h.log, "success", http.StatusOK, resp)
}

// GetOrder    godoc
// @Router       /order/{id} [GET]
// @Summary      Get order  by id
// @Description  get order  by id
// @Tags         order
// @Accept       json
// @Produce      json
// @Param        id path string true "order_id"
// @Success      201  {object}  models.Order
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetOrder(c *gin.Context) {
	id := c.Param("id")

	resp, err := h.services.OrderService().Get(context.Background(), &pbO.PrimaryKey{
		Id: id,
	})
	if err != nil {
		handleResponse(c, h.log, "error is while getting order by id", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "success", http.StatusOK, resp)
}

// GetOrderList godoc
// @Router       /orders  [GET]
// @Summary      Get order  list
// @Description  get order list
// @Tags         order
// @Accept       json
// @Produce      json
// @Param        page query string false "page"
// @Param        limit query string false "limit"
// @Param        order_id query string false "order_id"
// @Param        client_id query string false "client_id"
// @Param        branch_id query string false "branch_id"
// @Param        courier_id query string false "courier_id"
// @Param        from_price query number false "from_price"
// @Param        to_price query number false "to_price"
// @Param        status_type query string false "status_type"
// @Param        payment_type query string false "payment_type"
// @Success      201  {object}  models.OrdersResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetOrderList(c *gin.Context) {
	var (
		page, limit int
		err         error
	)

	pageStr := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pageStr)
	if err != nil {
		handleResponse(c, h.log, "error is while converting page", http.StatusBadRequest, err.Error())
		return
	}

	limitStr := c.DefaultQuery("limit", "10")
	limit, err = strconv.Atoi(limitStr)
	if err != nil {
		handleResponse(c, h.log, "error is while converting limit", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.OrderService().GetList(context.Background(), &pbO.OrderRequest{
		Page:        int32(page),
		Limit:       int32(limit),
		OrderId:     c.Query("order_id"),
		ClientId:    c.Query("client_id"),
		BranchId:    c.Query("branch_id"),
		CourierId:   c.Query("courier_id"),
		FromPrice:   float32(c.GetFloat64("from_price")),
		ToPrice:     float32(c.GetFloat64("to_price")),
		Type:        c.Query("type"),
		StatusType:  c.Query("status_type"),
		PaymentType: c.Query("payment_type"),
	})

	if err != nil {
		handleResponse(c, h.log, "error is while get order list", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "success", http.StatusOK, resp)
}

// UpdateOrder godoc
// @Router       /order/{id} [PUT]
// @Summary      Update order
// @Description  update order
// @Tags         order
// @Accept       json
// @Produce      json
// @Param        id path string true "order_id"
// @Param        order  body models.UpdateOrder false "order"
// @Success      201  {object}  models.Order
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateOrder(c *gin.Context) {
	request := pbO.Order{}
	uid := c.Param("id")

	if err := c.ShouldBindJSON(&request); err != nil {
		handleResponse(c, h.log, "error is while reading body", http.StatusBadRequest, err.Error())
		return
	}

	request.Id = uid

	resp, err := h.services.OrderService().Update(context.Background(), &request)
	if err != nil {
		handleResponse(c, h.log, "error is while getting order by id", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "updated", http.StatusOK, resp)
}

// DeleteOrder godoc
// @Router       /order/{id} [DELETE]
// @Summary      Delete order
// @Description  delete order
// @Tags         order
// @Accept       json
// @Produce      json
// @Param        id path string true "order_id"
// @Success      201  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteOrder(c *gin.Context) {
	uid := c.Param("id")

	if _, err := h.services.OrderService().Delete(context.Background(), &pbO.PrimaryKey{
		Id: uid,
	}); err != nil {
		handleResponse(c, h.log, "error is while delete order", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "deleted", http.StatusOK, "order deleted!")
}
