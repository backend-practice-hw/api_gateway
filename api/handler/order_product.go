package handler

import (
	pbO "api_gateway/genproto/order_service"
	"api_gateway/pkg/logger"
	"context"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

// CreateOrderProduct godoc
// @Router       /order_product [POST]
// @Summary      Create a new order_product
// @Description  Create a new order_product
// @Tags         order_product
// @Accept       json
// @Produce      json
// @Param        order_product  body models.CreateOrderProduct false "order_product"
// @Success      201  {object}  models.OrderProduct
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateOrderProduct(c *gin.Context) {
	request := pbO.OrderProduct{}
	if err := c.ShouldBindJSON(&request); err != nil {
		h.log.Error("error is while reading body", logger.Error(err))
		return
	}
	resp, err := h.services.OrderProductService().Create(context.Background(), &request)
	if err != nil {
		h.log.Error("error is while creating  order_product", logger.Error(err))
		return
	}
	handleResponse(c, h.log, "success", http.StatusOK, resp)
}

// GetOrderProduct    godoc
// @Router       /order_product/{id} [GET]
// @Summary      Get order_product  by id
// @Description  get order_product  by id
// @Tags         order_product
// @Accept       json
// @Produce      json
// @Param        id path string true "order_product_id"
// @Success      201  {object}  models.OrderProduct
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetOrderProduct(c *gin.Context) {
	id := c.Param("id")

	resp, err := h.services.OrderProductService().Get(context.Background(), &pbO.PrimaryKey{
		Id: id,
	})
	if err != nil {
		handleResponse(c, h.log, "error is while getting order_product by id", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "success", http.StatusOK, resp)
}

// GetOrderProductList godoc
// @Router       /order_products  [GET]
// @Summary      Get order_product  list
// @Description  get order_product list
// @Tags         order_product
// @Accept       json
// @Produce      json
// @Param        page query string false "page"
// @Param        limit query string false "limit"
// @Param        product_id query string false "product_id"
// @Param        from_price query number false "from_price"
// @Param        to_price query number false "to_price"
// @Param        order_id query string false "order_id"
// @Success      201  {object}  models.OrderProductsResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetOrderProductList(c *gin.Context) {
	var (
		page, limit int
		err         error
	)

	pageStr := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pageStr)
	if err != nil {
		handleResponse(c, h.log, "error is while converting page", http.StatusBadRequest, err.Error())
		return
	}

	limitStr := c.DefaultQuery("limit", "10")
	limit, err = strconv.Atoi(limitStr)
	if err != nil {
		handleResponse(c, h.log, "error is while converting limit", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.OrderProductService().GetList(context.Background(), &pbO.OrderProductRequest{
		Page:      int32(page),
		Limit:     int32(limit),
		ProductId: c.Query("product_id"),
		FromPrice: float32(c.GetFloat64("from_price")),
		ToPrice:   float32(c.GetFloat64("to_price")),
		OrderId:   c.Query("order_id"),
	})

	if err != nil {
		handleResponse(c, h.log, "error is while get order_product list", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "success", http.StatusOK, resp)
}

// UpdateOrderProduct godoc
// @Router       /order_product/{id} [PUT]
// @Summary      Update order_product
// @Description  update order_product
// @Tags         order_product
// @Accept       json
// @Produce      json
// @Param        id path string true "order_product_id"
// @Param        order  body models.UpdateOrderProduct false "order"
// @Success      201  {object}  models.OrderProduct
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateOrderProduct(c *gin.Context) {
	request := pbO.OrderProduct{}
	uid := c.Param("id")

	if err := c.ShouldBindJSON(&request); err != nil {
		handleResponse(c, h.log, "error is while reading body", http.StatusBadRequest, err.Error())
		return
	}

	request.Id = uid

	resp, err := h.services.OrderProductService().Update(context.Background(), &request)
	if err != nil {
		handleResponse(c, h.log, "error is while getting order by id", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "updated", http.StatusOK, resp)
}

// DeleteOrderProduct godoc
// @Router       /order_product/{id} [DELETE]
// @Summary      Delete order_product
// @Description  delete order_product
// @Tags         order_product
// @Accept       json
// @Produce      json
// @Param        id path string true "order_product_id"
// @Success      201  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteOrderProduct(c *gin.Context) {
	uid := c.Param("id")

	if _, err := h.services.OrderProductService().Delete(context.Background(), &pbO.PrimaryKey{
		Id: uid,
	}); err != nil {
		handleResponse(c, h.log, "error is while delete order", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "deleted", http.StatusOK, "order deleted!")
}
