package handler

import (
	"api_gateway/api/models"
	pbu "api_gateway/genproto/user_service"
	"api_gateway/pkg/logger"
	"context"
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

// CreateAdmin godoc
// @Router       /admin [POST]
// @Summary      Create a new admin
// @Description  Create a new admin
// @Tags         admin
// @Accept       json
// @Produce      json
// @Param        admin  body models.CreateAdmin false "admin"
// @Success      201  {object}  models.Admin
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateAdmin(c *gin.Context) {
	request := pbu.Admin{}
	if err := c.ShouldBindJSON(&request); err != nil {
		h.log.Error("error is while reading body", logger.Error(err))
		return
	}
	resp, err := h.services.AdminService().Create(context.Background(), &request)
	if err != nil {
		h.log.Error("error is while creating  admin", logger.Error(err))
		return
	}
	handleResponse(c, h.log, "success", http.StatusOK, resp)
}

// GetAdmin    godoc
// @Router       /admin/{id} [GET]
// @Summary      Get admin  by id
// @Description  get admin  by id
// @Tags         courier
// @Accept       json
// @Produce      json
// @Param        id path string true "admin_id"
// @Success      201  {object}  models.Admin
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetAdmin(c *gin.Context) {
	id := c.Param("id")

	resp, err := h.services.AdminService().Get(context.Background(), &pbu.PrimaryKey{
		Id: id,
	})
	if err != nil {
		handleResponse(c, h.log, "error is while getting admin by id", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "success", http.StatusOK, resp)
}

// GetAdminList godoc
// @Router       /admins  [GET]
// @Summary      Get admin  list
// @Description  get admin list
// @Tags         admin
// @Accept       json
// @Produce      json
// @Param        page query string false "page"
// @Param        limit query string false "limit"
// @Param        first_name query string false "first_name"
// @Param        last_name query string false "last_name"
// @Param        phone query string false "pone"
// @Param        from_created_at query string false "from_created_at"
// @Param        to_created_at query string false "to_created_at"
// @Success      201  {object}  models.AdminResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetAdminList(c *gin.Context) {
	var (
		page, limit int
		err         error
	)

	pageStr := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pageStr)
	if err != nil {
		handleResponse(c, h.log, "error is while converting page", http.StatusBadRequest, err.Error())
		return
	}

	limitStr := c.DefaultQuery("limit", "10")
	limit, err = strconv.Atoi(limitStr)
	if err != nil {
		handleResponse(c, h.log, "error is while converting limit", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.AdminService().GetList(context.Background(), &pbu.AdminRequest{
		Page:          int32(page),
		Limit:         int32(limit),
		FirstName:     c.Query("first_name"),
		LastName:      c.Query("last_name"),
		Phone:         c.Query("phone"),
		FromCreatedAt: c.Query("from_created_at"),
		ToCreatedAt:   c.Query("to_created_at"),
	})

	if err != nil {
		handleResponse(c, h.log, "error is while get client list", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "success", http.StatusOK, resp)
}

// UpdateAdmin godoc
// @Router       /admin/{id} [PUT]
// @Summary      Update admin
// @Description  update admin
// @Tags         admin
// @Accept       json
// @Produce      json
// @Param        id path string true "admin_id"
// @Param        admin  body models.UpdateAdmin false "admin"
// @Success      201  {object}  models.Admin
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateAdmin(c *gin.Context) {
	request := pbu.Admin{}
	uid := c.Param("id")

	if err := c.ShouldBindJSON(&request); err != nil {
		handleResponse(c, h.log, "error is while reading body", http.StatusBadRequest, err.Error())
		return
	}

	request.Id = uid

	resp, err := h.services.AdminService().Update(context.Background(), &request)
	if err != nil {
		handleResponse(c, h.log, "error is while getting admin by id", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "updated", http.StatusOK, resp)
}

// DeleteAdmin godoc
// @Router       /admin/{id} [DELETE]
// @Summary      Delete admin
// @Description  delete admin
// @Tags         admin
// @Accept       json
// @Produce      json
// @Param        id path string true "admin_id"
// @Success      201  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteAdmin(c *gin.Context) {
	uid := c.Param("id")

	if _, err := h.services.AdminService().Delete(context.Background(), &pbu.PrimaryKey{
		Id: uid,
	}); err != nil {
		handleResponse(c, h.log, "error is while delete admin", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "deleted", http.StatusOK, "order deleted!")
}

// ChangePasswordAdmin godoc
// @Router       /change_password_admin [PATCH]
// @Summary      Change admin password
// @Description  change admin password
// @Tags         admin
// @Accept       json
// @Produce      json
// @Param        change_password body models.ChangePassword false "change_password"
// @Success      201  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) ChangePasswordAdmin(c *gin.Context) {
	request := models.ChangePassword{}
	if err := c.ShouldBindJSON(&request); err != nil {
		handleResponse(c, h.log, "", http.StatusBadRequest, logger.Error(err))
		return
	}

	fmt.Println("request", &request)
	msg, err := h.services.AdminService().ChangePassword(context.Background(), &pbu.PasswordRequest{
		OldPassword: request.OldPassword,
		NewPassword: request.NewPassword,
	})

	if err != nil {
		handleResponse(c, h.log, "", http.StatusInternalServerError, logger.Error(err))
		return
	}

	handleResponse(c, h.log, "success", http.StatusOK, msg)
}
