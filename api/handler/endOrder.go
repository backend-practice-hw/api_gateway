package handler

import (
	pbO "api_gateway/genproto/order_service"
	"context"
	"github.com/gin-gonic/gin"
	"net/http"
)

// EndOrder godoc
// @Router       /end-order [PUT]
// @Summary      end order
// @Description  end order
// @Tags         order
// @Accept       json
// @Produce      json
// @Param 		 order body models.EndOrderRequest false "order"
// @Success      200  {object}  models.Order
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) EndOrder(c *gin.Context) {
	req := pbO.EndOrderRequest{}
	if err := c.ShouldBindJSON(&req); err != nil {
		handleResponse(c, h.log, "error is while reading body", http.StatusBadRequest, err)
		return
	}

	order, err := h.services.OrderService().EndOrder(context.Background(), &req)
	if err != nil {
		handleResponse(c, h.log, "error is while end order", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, h.log, "started order", http.StatusOK, order)
}
