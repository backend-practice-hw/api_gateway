package handler

import (
	"api_gateway/api/models"
	pbO "api_gateway/genproto/order_service"
	"context"
	"github.com/gin-gonic/gin"
	"net/http"
)

// StartOrder godoc
// @Router       /start-order [POST]
// @Summary      start-order
// @Description  start-order
// @Tags         order
// @Accept       json
// @Produce      json
// @Param 		 sell body models.CreateOrder false "sell"
// @Success      200  {object}  models.Order
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) StartOrder(c *gin.Context) {
	req := models.CreateOrder{}
	if err := c.ShouldBindJSON(&req); err != nil {
		handleResponse(c, h.log, "error is while reading body", http.StatusBadRequest, err)
		return
	}

	order, err := h.services.OrderService().StartOrder(context.Background(), &pbO.Order{
		ClientId:      req.ClientID,
		BranchId:      req.BranchID,
		Type:          req.Type,
		Address:       req.Address,
		CourierId:     req.CourierID,
		Price:         req.Price,
		DeliveryPrice: req.DeliveryPrice,
		Discount:      req.Discount,
		Status:        req.Status,
		PaymentType:   req.PaymentType,
	})
	if err != nil {
		handleResponse(c, h.log, "error is while starting order", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, h.log, "started order", http.StatusOK, order)
}
