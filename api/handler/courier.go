package handler

import (
	"api_gateway/api/models"
	pbu "api_gateway/genproto/user_service"
	"api_gateway/pkg/logger"
	"context"
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

// CreateCourier godoc
// @Router       /courier [POST]
// @Summary      Create a new courier
// @Description  Create a new courier
// @Tags         courier
// @Accept       json
// @Produce      json
// @Param        courier  body models.CreateCourier false "courier"
// @Success      201  {object}  models.Courier
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateCourier(c *gin.Context) {
	request := pbu.Courier{}
	if err := c.ShouldBindJSON(&request); err != nil {
		h.log.Error("error is while reading body", logger.Error(err))
		return
	}
	resp, err := h.services.CourierService().Create(context.Background(), &request)
	if err != nil {
		h.log.Error("error is while creating  courier", logger.Error(err))
		return
	}
	handleResponse(c, h.log, "success", http.StatusOK, resp)
}

// GetCourier    godoc
// @Router       /courier/{id} [GET]
// @Summary      Get courier  by id
// @Description  get courier  by id
// @Tags         courier
// @Accept       json
// @Produce      json
// @Param        id path string true "courier_id"
// @Success      201  {object}  models.Courier
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetCourier(c *gin.Context) {
	id := c.Param("id")

	resp, err := h.services.CourierService().Get(context.Background(), &pbu.PrimaryKey{
		Id: id,
	})
	if err != nil {
		handleResponse(c, h.log, "error is while getting courier by id", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "success", http.StatusOK, resp)
}

// GetCourierList godoc
// @Router       /couriers  [GET]
// @Summary      Get courier  list
// @Description  get courier list
// @Tags         courier
// @Accept       json
// @Produce      json
// @Param        page query string false "page"
// @Param        limit query string false "limit"
// @Param        first_name query string false "first_name"
// @Param        last_name query string false "last_name"
// @Param        phone query string false "pone"
// @Param        from_created_at query string false "from_created_at"
// @Param        to_created_at query string false "to_created_at"
// @Success      201  {object}  models.CourierResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetCourierList(c *gin.Context) {
	var (
		page, limit int
		err         error
	)

	pageStr := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pageStr)
	if err != nil {
		handleResponse(c, h.log, "error is while converting page", http.StatusBadRequest, err.Error())
		return
	}

	limitStr := c.DefaultQuery("limit", "10")
	limit, err = strconv.Atoi(limitStr)
	if err != nil {
		handleResponse(c, h.log, "error is while converting limit", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.CourierService().GetList(context.Background(), &pbu.CourierRequest{
		Page:          int32(page),
		Limit:         int32(limit),
		FirstName:     c.Query("first_name"),
		LastName:      c.Query("last_name"),
		Phone:         c.Query("phone"),
		FromCreatedAt: c.Query("from_created_at"),
		ToCreatedAt:   c.Query("to_created_at"),
	})

	if err != nil {
		handleResponse(c, h.log, "error is while get client list", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "success", http.StatusOK, resp)
}

// UpdateCourier godoc
// @Router       /courier/{id} [PUT]
// @Summary      Update courier
// @Description  update courier
// @Tags         courier
// @Accept       json
// @Produce      json
// @Param        id path string true "courier_id"
// @Param        client  body models.UpdateClient false "courier"
// @Success      201  {object}  models.Client
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateCourier(c *gin.Context) {
	request := pbu.Courier{}
	uid := c.Param("id")

	if err := c.ShouldBindJSON(&request); err != nil {
		handleResponse(c, h.log, "error is while reading body", http.StatusBadRequest, err.Error())
		return
	}

	request.Id = uid

	resp, err := h.services.CourierService().Update(context.Background(), &request)
	if err != nil {
		handleResponse(c, h.log, "error is while getting courier by id", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "updated", http.StatusOK, resp)
}

// DeleteCourier godoc
// @Router       /courier/{id} [DELETE]
// @Summary      Delete courier
// @Description  delete courier
// @Tags         courier
// @Accept       json
// @Produce      json
// @Param        id path string true "courier_id"
// @Success      201  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteCourier(c *gin.Context) {
	uid := c.Param("id")

	if _, err := h.services.CourierService().Delete(context.Background(), &pbu.PrimaryKey{
		Id: uid,
	}); err != nil {
		handleResponse(c, h.log, "error is while delete courier", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "deleted", http.StatusOK, "order deleted!")
}

// ChangePasswordCourier godoc
// @Router       /change_password_courier [PATCH]
// @Summary      Change courier password
// @Description  change courier password
// @Tags         courier
// @Accept       json
// @Produce      json
// @Param        change_password body models.ChangePassword false "change_password"
// @Success      201  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) ChangePasswordCourier(c *gin.Context) {
	request := models.ChangePassword{}
	if err := c.ShouldBindJSON(&request); err != nil {
		handleResponse(c, h.log, "", http.StatusBadRequest, logger.Error(err))
		return
	}

	fmt.Println("request", &request)
	msg, err := h.services.CourierService().ChangePassword(context.Background(), &pbu.PasswordRequest{
		Login:       request.Login,
		OldPassword: request.OldPassword,
		NewPassword: request.NewPassword,
	})

	if err != nil {
		handleResponse(c, h.log, "", http.StatusInternalServerError, logger.Error(err))
		return
	}

	handleResponse(c, h.log, "success", http.StatusOK, msg)
}
