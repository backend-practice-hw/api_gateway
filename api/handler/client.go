package handler

import (
	pbu "api_gateway/genproto/user_service"
	"api_gateway/pkg/logger"
	"context"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

// CreateClient godoc
// @Router       /client [POST]
// @Summary      Create a new client
// @Description  Create a new client
// @Tags         client
// @Accept       json
// @Produce      json
// @Param        client  body models.CreateClient false "client"
// @Success      201  {object}  models.Client
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateClient(c *gin.Context) {
	request := pbu.Client{}
	if err := c.ShouldBindJSON(&request); err != nil {
		h.log.Error("error is while reading body", logger.Error(err))
		return
	}
	resp, err := h.services.ClientService().Create(context.Background(), &request)
	if err != nil {
		h.log.Error("error is while creating  client", logger.Error(err))
		return
	}
	handleResponse(c, h.log, "success", http.StatusOK, resp)
}

// GetClient    godoc
// @Router       /client/{id} [GET]
// @Summary      Get client  by id
// @Description  get client  by id
// @Tags         client
// @Accept       json
// @Produce      json
// @Param        id path string true "client_id"
// @Success      201  {object}  models.Client
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetClient(c *gin.Context) {
	id := c.Param("id")

	resp, err := h.services.ClientService().Get(context.Background(), &pbu.PrimaryKey{
		Id: id,
	})
	if err != nil {
		handleResponse(c, h.log, "error is while getting client by id", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "success", http.StatusOK, resp)
}

// GetClientList godoc
// @Router       /clients  [GET]
// @Summary      Get client  list
// @Description  get client list
// @Tags         client
// @Accept       json
// @Produce      json
// @Param        page query string false "page"
// @Param        limit query string false "limit"
// @Param        first_name query string false "first_name"
// @Param        last_name query string false "last_name"
// @Param        phone query string false "pone"
// @Param        from_created_at query string false "from_created_at"
// @Param        to_created_at query string false "to_created_at"
// @Param        from_last_ordered_date query string false "from_last_ordered_date"
// @Param        to_last_ordered_date query string false "to_last_ordered_date"
// @Param        from_total_orders_sum query number false "from_total_orders_sum"
// @Param        to_total_orders_sum query number false "to_total_orders_sum"
// @Param        from_total_orders_count query int false "from_total_orders_count"
// @Param        to_total_orders_count query int false "to_total_orders_count"
// @Param        from_discount_amount query number false "from_discount_amount"
// @Param        to_discount_amount query number false "to_discount_amount"
// @Param        discount_type query string false "discount_type"
// @Success      201  {object}  models.ClientResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetClientList(c *gin.Context) {
	var (
		page, limit int
		err         error
	)

	pageStr := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pageStr)
	if err != nil {
		handleResponse(c, h.log, "error is while converting page", http.StatusBadRequest, err.Error())
		return
	}

	limitStr := c.DefaultQuery("limit", "10")
	limit, err = strconv.Atoi(limitStr)
	if err != nil {
		handleResponse(c, h.log, "error is while converting limit", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.ClientService().GetList(context.Background(), &pbu.ClientRequest{
		Page:                 int32(page),
		Limit:                int32(limit),
		FirstName:            c.Query("first_name"),
		LastName:             c.Query("last_name"),
		Phone:                c.Query("phone"),
		FromCreatedAt:        c.Query("from_created_at"),
		ToCreatedAt:          c.Query("to_created_at"),
		FromLastOrderedDate:  c.Query("from_last_ordered_date"),
		ToLastOrderedDate:    c.Query("to_last_ordered_date"),
		FromTotalOrdersSum:   float32(c.GetFloat64("from_total_orders_sum")),
		ToTotalOrdersSum:     float32(c.GetFloat64("to_total_orders_sum")),
		FromTotalOrdersCount: int32(c.GetInt64("from_total_orders_count")),
		ToTotalOrdersCount:   int32(c.GetInt64("to_total_orders_count")),
		FromDiscountAmount:   float32(c.GetFloat64("from_discount_amount")),
		ToDiscountAmount:     float32(c.GetFloat64("to_discount_amount")),
		DiscountType:         c.Query("discount_type"),
	})

	if err != nil {
		handleResponse(c, h.log, "error is while get client list", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "success", http.StatusOK, resp)
}

// UpdateClient godoc
// @Router       /client/{id} [PUT]
// @Summary      Update client
// @Description  update client
// @Tags         client
// @Accept       json
// @Produce      json
// @Param        id path string true "order_id"
// @Param        client  body models.UpdateClient false "client"
// @Success      201  {object}  models.Client
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateClient(c *gin.Context) {
	request := pbu.Client{}
	uid := c.Param("id")

	if err := c.ShouldBindJSON(&request); err != nil {
		handleResponse(c, h.log, "error is while reading body", http.StatusBadRequest, err.Error())
		return
	}

	request.Id = uid

	resp, err := h.services.ClientService().Update(context.Background(), &request)
	if err != nil {
		handleResponse(c, h.log, "error is while getting client by id", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "updated", http.StatusOK, resp)
}

// DeleteClient godoc
// @Router       /client/{id} [DELETE]
// @Summary      Delete client
// @Description  delete client
// @Tags         client
// @Accept       json
// @Produce      json
// @Param        id path string true "client_id"
// @Success      201  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteClient(c *gin.Context) {
	uid := c.Param("id")

	if _, err := h.services.ClientService().Delete(context.Background(), &pbu.PrimaryKey{
		Id: uid,
	}); err != nil {
		handleResponse(c, h.log, "error is while delete client", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "deleted", http.StatusOK, "order deleted!")
}
