package handler

import (
	pbC "api_gateway/genproto/catalog_service"
	"api_gateway/pkg/logger"
	"context"
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

// CreateProduct godoc
// @Router       /product [POST]
// @Summary      Create a new product
// @Description  Create a new product
// @Tags         product
// @Accept       json
// @Produce      json
// @Param        product body models.CreateProduct false "product"
// @Success      201  {object}  models.Product
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateProduct(c *gin.Context) {
	request := pbC.CreateProduct{}
	if err := c.ShouldBindJSON(&request); err != nil {
		h.log.Error("error is while reading body", logger.Error(err))
		return
	}
	product, err := h.services.ProductService().Create(context.Background(), &request)
	if err != nil {
		fmt.Println("err", err.Error())
		h.log.Error("error is while creating category", logger.Error(err))
		return
	}
	handleResponse(c, h.log, "success", http.StatusOK, product)
}

// GetProduct   godoc
// @Router       /product/{id} [GET]
// @Summary      Get product by id
// @Description  get product by id
// @Tags         product
// @Accept       json
// @Produce      json
// @Param        id path string true "product_id"
// @Success      201  {object}  models.Product
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetProduct(c *gin.Context) {
	id := c.Param("id")

	resp, err := h.services.ProductService().Get(context.Background(), &pbC.PrimaryKey{
		Id: id,
	})
	if err != nil {
		handleResponse(c, h.log, "error is while getting product by id", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "", http.StatusOK, resp)
}

// GetProductList godoc
// @Router       /product [GET]
// @Summary      Get product list
// @Description  get product list
// @Tags         product
// @Accept       json
// @Produce      json
// @Param        page query string false "page"
// @Param        limit query string false "limit"
// @Param        title query string false "title"
// @Param        category_id query string false "category_id"
// @Param        active query string false "active"
// @Param        type query string false "type"
// @Success      201  {object}  models.ProductResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetProductList(c *gin.Context) {
	var (
		page, limit                           int
		title, categoryID, typeStr, activeStr string
		active                                bool
		err                                   error
	)

	pageStr := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pageStr)
	if err != nil {
		handleResponse(c, h.log, "error is while converting page", http.StatusBadRequest, err.Error())
		return
	}

	limitStr := c.DefaultQuery("limit", "10")
	limit, err = strconv.Atoi(limitStr)
	if err != nil {
		handleResponse(c, h.log, "error is while converting limit", http.StatusBadRequest, err.Error())
		return
	}

	title = c.Query("title")
	categoryID = c.Query("category_id")

	// active = c.GetBool("active")

	activeStr = c.Query("active")
	if activeStr != "" {
		active, err = strconv.ParseBool(activeStr)
		if err != nil {
			handleResponse(c, h.log, "error is while converting active", http.StatusBadRequest, err.Error())
			return
		}
	}

	typeStr = c.Query("type")

	products, err := h.services.ProductService().GetList(context.Background(), &pbC.GetProductRequest{
		Page:       int32(page),
		Limit:      int32(limit),
		Title:      title,
		CategoryId: categoryID,
		Active:     active,
		Type:       typeStr,
	})

	if err != nil {
		handleResponse(c, h.log, "error is while get category list", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "", http.StatusOK, products)
}

// UpdateProduct godoc
// @Router       /product/{id} [PUT]
// @Summary      Update product
// @Description  update product
// @Tags         product
// @Accept       json
// @Produce      json
// @Param        id path string true "product_id"
// @Param        product body models.UpdateProduct false "product"
// @Success      201  {object}  models.Product
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateProduct(c *gin.Context) {
	product := pbC.Product{}
	uid := c.Param("id")

	if err := c.ShouldBindJSON(&product); err != nil {
		handleResponse(c, h.log, "error is while reading body", http.StatusBadRequest, err.Error())
		return
	}

	product.Id = uid

	updatedProduct, err := h.services.ProductService().Update(context.Background(), &product)
	if err != nil {
		handleResponse(c, h.log, "error is while getting product by id", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "", http.StatusOK, updatedProduct)
}

// DeleteProduct godoc
// @Router       /product/{id} [DELETE]
// @Summary      Delete product
// @Description  delete product
// @Tags         product
// @Accept       json
// @Produce      json
// @Param        id path string true "product_id"
// @Success      201  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteProduct(c *gin.Context) {
	uid := c.Param("id")

	if _, err := h.services.ProductService().Delete(context.Background(), &pbC.PrimaryKey{
		Id: uid,
	}); err != nil {
		handleResponse(c, h.log, "error is while delete product", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "", http.StatusOK, "product deleted!")
}
