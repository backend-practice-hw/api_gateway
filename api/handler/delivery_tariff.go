package handler

import (
	pbO "api_gateway/genproto/order_service"
	"api_gateway/pkg/logger"
	"context"
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

// CreateDeliveryTariff godoc
// @Router       /delivery_tariff [POST]
// @Summary      Create a new delivery_tariff
// @Description  Create a new delivery_tariff
// @Tags         delivery_tariff
// @Accept       json
// @Produce      json
// @Param        delivery_tariff  body models.CreateDeliveryTariff false "delivery_tariff"
// @Success      201  {object}  models.DeliveryTariff
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateDeliveryTariff(c *gin.Context) {
	request := pbO.DeliveryTariff{}
	if err := c.ShouldBindJSON(&request); err != nil {
		h.log.Error("error is while reading body", logger.Error(err))
		return
	}
	resp, err := h.services.DeliveryTariffService().Create(context.Background(), &request)
	if err != nil {
		fmt.Println("err", err.Error())
		h.log.Error("error is while creating delivery tariff", logger.Error(err))
		return
	}
	handleResponse(c, h.log, "success", http.StatusOK, resp)
}

// GetDeliveryTariff    godoc
// @Router       /delivery_tariff/{id} [GET]
// @Summary      Get delivery_tariff  by id
// @Description  get delivery_tariff  by id
// @Tags         delivery_tariff
// @Accept       json
// @Produce      json
// @Param        id path string true "delivery_tariff_id"
// @Success      201  {object}  models.DeliveryTariff
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetDeliveryTariff(c *gin.Context) {
	id := c.Param("id")

	resp, err := h.services.DeliveryTariffService().Get(context.Background(), &pbO.PrimaryKey{
		Id: id,
	})
	if err != nil {
		handleResponse(c, h.log, "error is while getting delivery tariff by id", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "success", http.StatusOK, resp)
}

// GetDeliveryTariffList godoc
// @Router       /delivery_tariffs  [GET]
// @Summary      Get delivery_tariff  list
// @Description  get delivery_tariff list
// @Tags         delivery_tariff
// @Accept       json
// @Produce      json
// @Param        page query string false "page"
// @Param        limit query string false "limit"
// @Param        name query string false "name"
// @Param        type query string false "type"
// @Success      201  {object}  models.DeliveryTariffResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetDeliveryTariffList(c *gin.Context) {
	var (
		page, limit int
		err         error
	)

	pageStr := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pageStr)
	if err != nil {
		handleResponse(c, h.log, "error is while converting page", http.StatusBadRequest, err.Error())
		return
	}

	limitStr := c.DefaultQuery("limit", "10")
	limit, err = strconv.Atoi(limitStr)
	if err != nil {
		handleResponse(c, h.log, "error is while converting limit", http.StatusBadRequest, err.Error())
		return
	}

	name := c.Query("name")
	typeStr := c.Query("type")

	resp, err := h.services.DeliveryTariffService().GetList(context.Background(), &pbO.DeliveryTariffRequest{
		Page:  int32(page),
		Limit: int32(limit),
		Name:  name,
		Type:  typeStr,
	})

	if err != nil {
		handleResponse(c, h.log, "error is while get alternative value list", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "success", http.StatusOK, resp)
}

// UpdateDeliveryTariff godoc
// @Router       /delivery_tariff/{id} [PUT]
// @Summary      Update delivery_tariff
// @Description  update delivery_tariff
// @Tags         delivery_tariff
// @Accept       json
// @Produce      json
// @Param        id path string true "delivery_tariff_id"
// @Param        delivery_tariff  body models.UpdateDeliveryTariff false "delivery_tariff"
// @Success      201  {object}  models.DeliveryTariff
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateDeliveryTariff(c *gin.Context) {
	request := pbO.DeliveryTariff{}
	uid := c.Param("id")

	if err := c.ShouldBindJSON(&request); err != nil {
		handleResponse(c, h.log, "error is while reading body", http.StatusBadRequest, err.Error())
		return
	}

	request.Id = uid

	resp, err := h.services.DeliveryTariffService().Update(context.Background(), &request)
	if err != nil {
		handleResponse(c, h.log, "error is while getting delivery tariff by id", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "updated", http.StatusOK, resp)
}

// DeleteDeliveryTariff godoc
// @Router       /delivery_tariff/{id} [DELETE]
// @Summary      Delete delivery_tariff
// @Description  delete delivery_tariff
// @Tags         delivery_tariff
// @Accept       json
// @Produce      json
// @Param        id path string true "delivery_tariff_id"
// @Success      201  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteDeliveryTariff(c *gin.Context) {
	uid := c.Param("id")

	if _, err := h.services.DeliveryTariffService().Delete(context.Background(), &pbO.PrimaryKey{
		Id: uid,
	}); err != nil {
		handleResponse(c, h.log, "error is while delete delivery tariff", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "deleted", http.StatusOK, "delivery tariff deleted!")
}
