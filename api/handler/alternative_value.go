package handler

import (
	pbO "api_gateway/genproto/order_service"
	"api_gateway/pkg/logger"
	"context"
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

// CreateAlternativeValue godoc
// @Router       /alternative-value [POST]
// @Summary      Create a new alternative-value
// @Description  Create a new alternative-value
// @Tags         alternative-value
// @Accept       json
// @Produce      json
// @Param        alternative-value  body models.CreateAlternativeValue false "alternative-value "
// @Success      201  {object}  models.AlternativeValue
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateAlternativeValue(c *gin.Context) {
	request := pbO.AlternativeValue{}
	if err := c.ShouldBindJSON(&request); err != nil {
		h.log.Error("error is while reading body", logger.Error(err))
		return
	}
	resp, err := h.services.AlternativeValueService().Create(context.Background(), &request)
	if err != nil {
		fmt.Println("err", err.Error())
		h.log.Error("error is while creating alternative value", logger.Error(err))
		return
	}
	handleResponse(c, h.log, "success", http.StatusOK, resp)
}

// GetCategory    godoc
// @Router       /alternative-value/{id} [GET]
// @Summary      Get alternative-value  by id
// @Description  get alternative-value  by id
// @Tags         alternative-value
// @Accept       json
// @Produce      json
// @Param        id path string true "alternative_value_id"
// @Success      201  {object}  models.AlternativeValue
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetAlternativeValue(c *gin.Context) {
	id := c.Param("id")

	resp, err := h.services.AlternativeValueService().Get(context.Background(), &pbO.PrimaryKey{
		Id: id,
	})
	if err != nil {
		handleResponse(c, h.log, "error is while getting alternative value by id", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "success", http.StatusOK, resp)
}

// GetCategoryList godoc
// @Router       /alternative-values  [GET]
// @Summary      Get alternative-value  list
// @Description  get alternative-value  list
// @Tags         alternative-value
// @Accept       json
// @Produce      json
// @Param        page query string false "page"
// @Param        limit query string false "limit"
// @Param        from_price query number false "from_price"
// @Param        to_price query number false "to_price"
// @Success      201  {object}  models.AlternativeValueResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetAlternativeValueList(c *gin.Context) {
	var (
		page, limit int
		err         error
	)

	pageStr := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pageStr)
	if err != nil {
		handleResponse(c, h.log, "error is while converting page", http.StatusBadRequest, err.Error())
		return
	}

	limitStr := c.DefaultQuery("limit", "10")
	limit, err = strconv.Atoi(limitStr)
	if err != nil {
		handleResponse(c, h.log, "error is while converting limit", http.StatusBadRequest, err.Error())
		return
	}

	fromPrice := c.GetFloat64("from_price")
	toPrice := c.GetFloat64("to_price")

	resp, err := h.services.AlternativeValueService().GetList(context.Background(), &pbO.GetAlternativeValueRequest{
		Page:      int32(page),
		Limit:     int32(limit),
		FromPrice: float32(fromPrice),
		ToPrice:   float32(toPrice),
	})

	if err != nil {
		handleResponse(c, h.log, "error is while get alternative value list", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "success", http.StatusOK, resp)
}

// UpdateAlternativeValue godoc
// @Router       /alternative-value/{id} [PUT]
// @Summary      Update alternative-value
// @Description  update alternative-value
// @Tags         alternative-value
// @Accept       json
// @Produce      json
// @Param        id path string true "alternative-value "
// @Param        alternative-value  body models.UpdateAlternativeValue false "alternative-value "
// @Success      201  {object}  models.AlternativeValue
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateAlternativeValue(c *gin.Context) {
	request := pbO.AlternativeValue{}
	uid := c.Param("id")

	if err := c.ShouldBindJSON(&request); err != nil {
		handleResponse(c, h.log, "error is while reading body", http.StatusBadRequest, err.Error())
		return
	}

	request.Id = uid

	resp, err := h.services.AlternativeValueService().Update(context.Background(), &request)
	if err != nil {
		handleResponse(c, h.log, "error is while getting alter value by id", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "updated", http.StatusOK, resp)
}

// DeleteAlternativeValue godoc
// @Router       /alternative-value/{id} [DELETE]
// @Summary      Delete alternative-value
// @Description  delete alternative-value
// @Tags         alternative-value
// @Accept       json
// @Produce      json
// @Param        id path string true "alternative_value_id"
// @Success      201  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteAlternativeValue(c *gin.Context) {
	uid := c.Param("id")

	if _, err := h.services.AlternativeValueService().Delete(context.Background(), &pbO.PrimaryKey{
		Id: uid,
	}); err != nil {
		handleResponse(c, h.log, "error is while delete alternative value", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "deleted", http.StatusOK, "alternative val deleted!")
}
