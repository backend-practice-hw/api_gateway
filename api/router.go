package api

import (
	_ "api_gateway/api/docs"
	"api_gateway/api/handler"
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

// New ...
// @title           Swagger Example API
// @version         1.0
// @description     This is a sample server celler server.
// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
func New(h handler.Handler) *gin.Engine {
	r := gin.New()

	// category
	r.POST("/category", h.CreateCategory)
	r.GET("/category/:id", h.GetCategory)
	r.GET("/categories", h.GetCategoryList)
	r.PUT("/category/:id", h.UpdateCategory)
	r.DELETE("/category/:id", h.DeleteCategory)

	// product
	r.POST("/product", h.CreateProduct)
	r.GET("/product/:id", h.GetProduct)
	r.GET("/product", h.GetProductList)
	r.PUT("/product/:id", h.UpdateProduct)
	r.DELETE("/product/:id", h.DeleteProduct)

	// client
	r.POST("/client", h.CreateClient)
	r.GET("/client/:id", h.GetClient)
	r.GET("/clients", h.GetClientList)
	r.PUT("/client/:id", h.UpdateClient)
	r.DELETE("/client/:id", h.DeleteClient)

	// admin
	r.POST("/admin", h.CreateAdmin)
	r.GET("/admin/:id", h.GetAdmin)
	r.GET("/admins", h.GetAdminList)
	r.PUT("/admin/:id", h.UpdateAdmin)
	r.DELETE("/admin/:id", h.DeleteAdmin)
	r.PATCH("/change_password_admin", h.ChangePasswordAdmin)

	// courier
	r.POST("/courier", h.CreateCourier)
	r.GET("/courier/:id", h.GetCourier)
	r.GET("/couriers", h.GetCourierList)
	r.PUT("/courier/:id", h.UpdateCourier)
	r.DELETE("/courier/:id", h.DeleteCourier)
	r.PATCH("/change_password_courier", h.ChangePasswordCourier)

	// branch
	r.POST("/branch", h.CreateBranch)
	r.GET("/branch/:id", h.GetBranch)
	r.GET("/branches", h.GetBranchList)
	r.PUT("/branch/:id", h.UpdateBranch)
	r.DELETE("/branch/:id", h.DeleteBranch)
	r.GET("/branches-data", h.GetBranchesList)

	// order
	r.POST("/order", h.CreateOrder)
	r.GET("/order/:id", h.GetOrder)
	r.GET("/orders", h.GetOrderList)
	r.PUT("/order/:id", h.UpdateOrder)
	r.DELETE("/order/:id", h.DeleteOrder)
	r.POST("/start-order", h.StartOrder)
	r.PUT("/end-order", h.EndOrder)

	// order-product
	r.POST("/order_product", h.CreateOrderProduct)
	r.GET("/order_product/:id", h.GetOrderProduct)
	r.GET("/order_products", h.GetOrderProductList)
	r.PUT("/order_product/:id", h.UpdateOrderProduct)
	r.DELETE("/order_product/:id", h.DeleteOrderProduct)

	// delivery_tariff
	r.POST("/delivery_tariff", h.CreateDeliveryTariff)
	r.GET("/delivery_tariff/:id", h.GetDeliveryTariff)
	r.GET("/delivery_tariffs", h.GetDeliveryTariffList)
	r.PUT("/delivery_tariff/:id", h.UpdateDeliveryTariff)
	r.DELETE("/delivery_tariff/:id", h.DeleteDeliveryTariff)

	// alternative_value
	r.POST("/alternative_value", h.CreateAlternativeValue)
	r.GET("/alternative_value/:id", h.GetAlternativeValue)
	r.GET("/alternative_value", h.GetAlternativeValueList)
	r.PUT("/alternative_value/:id", h.UpdateAlternativeValue)
	r.DELETE("/alternative_value/:id", h.DeleteAlternativeValue)

	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	return r
}
