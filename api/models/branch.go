package models

type Branch struct {
	ID               string `json:"id"`
	Name             string `json:"name"`
	Phone            string `json:"phone"`
	Photo            string `json:"photo"`
	DeliveryTariffID string `json:"delivery_tariff_id"`
	WorkHourStart    string `json:"work_hour_start"`
	WorkHourEnd      string `json:"work_hour_end"`
	Address          string `json:"address"`
	Destination      string `json:"destination"`
	Active           bool   `json:"active"`
	CreatedAt        string `json:"created_at"`
	UpdatedAt        string `json:"updated_at"`
}

type CreateBranch struct {
	Name             string `json:"name"`
	Phone            string `json:"phone"`
	Photo            string `json:"photo"`
	DeliveryTariffID string `json:"delivery_tariff_id"`
	WorkHourStart    string `json:"work_hour_start"`
	WorkHourEnd      string `json:"work_hour_end"`
	Address          string `json:"address"`
	Destination      string `json:"destination"`
	Active           bool   `json:"active"`
}

type UpdateBranch struct {
	Name             string `json:"name"`
	Phone            string `json:"phone"`
	Photo            string `json:"photo"`
	DeliveryTariffID string `json:"delivery_tariff_id"`
	WorkHourStart    string `json:"work_hour_start"`
	WorkHourEnd      string `json:"work_hour_end"`
	Address          string `json:"address"`
	Destination      string `json:"destination"`
	Active           bool   `json:"active"`
}

type BranchRequest struct {
	Page          int32  `json:"page"`
	Limit         int32  `json:"limit"`
	Name          string `json:"name"`
	FromCreatedAt string `json:"from_created_at"`
	ToCreatedAt   string `json:"to_created_at"`
}

type BranchesResponse struct {
	Branches []Branch `json:"branches"`
	Count    int32    `json:"count"`
}
