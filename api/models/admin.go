package models

type Admin struct {
	ID        string `json:"id"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Phone     string `json:"phone"`
	Active    bool   `json:"active"`
	Login     string `json:"login"`
	Password  string `json:"password"`
	CreatedAt string `json:"created_at"`
	UpdatedAt string `json:"updated_at"`
}

type CreateAdmin struct {
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Phone     string `json:"phone"`
	Active    bool   `json:"active"`
	Login     string `json:"login"`
	Password  string `json:"password"`
}

type UpdateAdmin struct {
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Phone     string `json:"phone"`
	Active    bool   `json:"active"`
	Login     string `json:"login"`
	Password  string `json:"password"`
}

type AdminRequest struct {
	Page          int32  `json:"page"`
	Limit         int32  `json:"limit"`
	FirstName     string `json:"first_name"`
	LastName      string `json:"last_name"`
	Phone         string `json:"phone"`
	FromCreatedAt string `json:"from_created_at"`
	ToCreatedAt   string `json:"to_created_at"`
}

type AdminResponse struct {
	Admins []Admin `json:"admins"`
	Count  int32   `json:"count"`
}
