package models

type Client struct {
	ID               string  `json:"id"`
	FirstName        string  `json:"first_name"`
	LastName         string  `json:"last_name"`
	Phone            string  `json:"phone"`
	DateOfBirth      string  `json:"date_of_birth"`
	LastOrderedDate  string  `json:"last_ordered_date"`
	TotalOrdersSum   float32 `json:"total_orders_sum"`
	TotalOrdersCount int     `json:"total_orders_count"`
	DiscountType     string  `json:"discount_type"`
	DiscountAmount   float32 `json:"discount_amount"`
	CreatedAt        string  `json:"created_at"`
	UpdatedAt        string  `json:"updated_at"`
}

type CreateClient struct {
	FirstName        string  `json:"first_name"`
	LastName         string  `json:"last_name"`
	Phone            string  `json:"phone"`
	DateOfBirth      string  `json:"date_of_birth"`
	LastOrderedDate  string  `json:"last_ordered_date"`
	TotalOrdersSum   float32 `json:"total_orders_sum"`
	TotalOrdersCount int     `json:"total_orders_count"`
	DiscountType     string  `json:"discount_type"`
	DiscountAmount   float32 `json:"discount_amount"`
}

type UpdateClient struct {
	FirstName        string  `json:"first_name"`
	LastName         string  `json:"last_name"`
	Phone            string  `json:"phone"`
	DateOfBirth      string  `json:"date_of_birth"`
	LastOrderedDate  string  `json:"last_ordered_date"`
	TotalOrdersSum   float32 `json:"total_orders_sum"`
	TotalOrdersCount int     `json:"total_orders_count"`
	DiscountType     string  `json:"discount_type"`
	DiscountAmount   float32 `json:"discount_amount"`
}

type ClientRequest struct {
	Page                 int32   `json:"page"`
	Limit                int32   `json:"limit"`
	FirstName            string  `json:"first_name"`
	LastName             string  `json:"last_name"`
	Phone                string  `json:"phone"`
	FromCreatedAt        string  `json:"from_created_at"`
	ToCreatedAt          string  `json:"to_created_at"`
	FromLastOrderedDate  string  `json:"from_last_ordered_date"`
	ToLastOrderedDate    string  `json:"to_last_ordered_date"`
	FromTotalOrdersSum   float32 `json:"from_total_orders_sum"`
	ToTotalOrdersSum     float32 `json:"to_total_orders_sum"`
	FromTotalOrdersCount int     `json:"from_total_orders_count"`
	ToTotalOrdersCount   int     `json:"to_total_orders_count"`
	FromDiscountAmount   float32 `json:"from_discount_amount"`
	ToDiscountAmount     float32 `json:"to_discount_amount"`
	DiscountType         string  `json:"discount_type"`
}

type ClientResponse struct {
	Clients []Client `json:"clients"`
	Count   int32    `json:"count"`
}
