package models

type Order struct {
	ID            string  `json:"id"`
	OrderID       string  `json:"order_id"`
	ClientID      string  `json:"client_id"`
	BranchID      string  `json:"branch_id"`
	Type          string  `json:"type"`
	Address       string  `json:"address"`
	CourierID     string  `json:"courier_id"`
	Price         float32 `json:"price"`
	DeliveryPrice float32 `json:"delivery_price"`
	Discount      float32 `json:"discount"`
	Status        string  `json:"status"`
	PaymentType   string  `json:"payment_type"`
	CreatedAt     string  `json:"created_at"`
	UpdatedAt     string  `json:"updated_at"`
}

type CreateOrder struct {
	OrderID       string  `json:"order_id"`
	ClientID      string  `json:"client_id"`
	BranchID      string  `json:"branch_id"`
	Type          string  `json:"type"`
	Address       string  `json:"address"`
	CourierID     string  `json:"courier_id"`
	Price         float32 `json:"price"`
	DeliveryPrice float32 `json:"delivery_price"`
	Discount      float32 `json:"discount"`
	Status        string  `json:"status"`
	PaymentType   string  `json:"payment_type"`
}

type UpdateOrder struct {
	OrderID       string  `json:"order_id"`
	ClientID      string  `json:"client_id"`
	BranchID      string  `json:"branch_id"`
	Type          string  `json:"type"`
	Address       string  `json:"address"`
	CourierID     string  `json:"courier_id"`
	Price         float32 `json:"price"`
	DeliveryPrice float32 `json:"delivery_price"`
	Discount      float32 `json:"discount"`
	Status        string  `json:"status"`
	PaymentType   string  `json:"payment_type"`
}

type OrderRequest struct {
	Page        int     `json:"page"`
	Limit       int     `json:"limit"`
	OrderID     string  `json:"order_id"`
	ClientID    string  `json:"client_id"`
	BranchID    string  `json:"branch_id"`
	CourierID   string  `json:"courier_id"`
	FromPrice   float32 `json:"from_price"`
	ToPrice     float32 `json:"to_price"`
	Type        string  `json:"type"`
	StatusType  string  `json:"status_type"`
	PaymentType string  `json:"payment_type"`
}

type OrdersResponse struct {
	Orders []Order `json:"orders"`
	Count  int     `json:"count"`
}

type EndOrderRequest struct {
	ID       string  `json:"id"`
	ClientID string  `json:"client_id"`
	BranchID string  `json:"branch_id"`
	Discount float32 `json:"discount"`
}
