package models

type OrderProduct struct {
	ID        string  `json:"id"`
	ProductID string  `json:"product_id"`
	Quantity  int     `json:"quantity"`
	Price     float32 `json:"price"`
	OrderID   string  `json:"order_id"`
	CreatedAt string  `json:"created_at"`
	UpdatedAt string  `json:"updated_at"`
}

type CreateOrderProduct struct {
	ProductID string  `json:"product_id"`
	Quantity  int     `json:"quantity"`
	Price     float32 `json:"price"`
	OrderID   string  `json:"order_id"`
}

type UpdateOrderProduct struct {
	ProductID string  `json:"product_id"`
	Quantity  int     `json:"quantity"`
	Price     float32 `json:"price"`
	OrderID   string  `json:"order_id"`
}

type OrderProductRequest struct {
	Page      int     `json:"page"`
	Limit     int     `json:"limit"`
	ProductID string  `json:"product_id"`
	FromPrice float32 `json:"from_price"`
	ToPrice   float32 `json:"to_price"`
	OrderID   string  `json:"order_id"`
}

type OrderProductsResponse struct {
	OrderProducts []OrderProduct `json:"order_products"`
	Count         int            `json:"count"`
}
