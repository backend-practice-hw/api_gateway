package models

type DeliveryTariff struct {
	ID               string  `json:"id"`
	Name             string  `json:"name"`
	Type             string  `json:"type"`
	BasePrice        float32 `json:"base_price"`
	AlternativeValue string  `json:"alternative_value"`
	CreatedAt        string  `json:"created_at"`
	UpdatedAt        string  `json:"updated_at"`
}

type CreateDeliveryTariff struct {
	Name             string  `json:"name"`
	Type             string  `json:"type"`
	BasePrice        float32 `json:"base_price"`
	AlternativeValue string  `json:"alternative_value"`
}

type UpdateDeliveryTariff struct {
	Name             string  `json:"name"`
	Type             string  `json:"type"`
	BasePrice        float32 `json:"base_price"`
	AlternativeValue string  `json:"alternative_value"`
}

type DeliveryTariffRequest struct {
	Page  int    `json:"page"`
	Limit int    `json:"limit"`
	Name  string `json:"name"`
	Type  string `json:"type"`
}

type DeliveryTariffResponse struct {
	DeliveryTariffs []DeliveryTariff `json:"delivery_tariffs"`
	Count           int              `json:"count"`
}
