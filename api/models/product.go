package models

type Product struct {
	ID          string  `json:"id"`
	Title       string  `json:"title"`
	Description string  `json:"description"`
	Photos      string  `json:"photos"`
	OrderNumber int32   `json:"order_number"`
	Active      bool    `json:"active"`
	Type        string  `json:"type"`
	Price       float32 `json:"price"`
	CategoryID  string  `json:"category_id"`
	CreatedAt   string  `json:"created_at"`
	UpdatedAt   string  `json:"updated_at"`
}

type CreateProduct struct {
	Title       string  `json:"title"`
	Description string  `json:"description"`
	Photos      string  `json:"photos"`
	Active      bool    `json:"active"`
	Type        string  `json:"type"`
	Price       float32 `json:"price"`
	CategoryID  string  `json:"category_id"`
}

type UpdateProduct struct {
	ID          string  `json:"-"`
	Title       string  `json:"title"`
	Description string  `json:"description"`
	Photos      string  `json:"photos"`
	Active      bool    `json:"active"`
	Type        string  `json:"type"`
	Price       float32 `json:"price"`
	CategoryID  string  `json:"category_id"`
}

type ProductRequest struct {
	Page       int32  `json:"page"`
	Limit      int32  `json:"limit"`
	Title      string `json:"title"`
	CategoryID string `json:"category_id"`
	Active     bool   `json:"active"`
	Type       string `json:"type"`
}

type ProductResponse struct {
	Products []Product `json:"products"`
	Count    int32     `json:"count"`
}
