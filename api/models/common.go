package models

type ChangePassword struct {
	Login       string `json:"login"`
	NewPassword string `json:"new_password"`
	OldPassword string `json:"old_password"`
}
