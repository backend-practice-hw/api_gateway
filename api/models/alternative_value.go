package models

type CreateAlternativeValue struct {
	FromPrice float32 `json:"from_price"`
	ToPrice   float32 `json:"to_price"`
	Price     float32 `json:"price"`
}

type AlternativeValue struct {
	ID        string  `json:"id"`
	FromPrice float32 `json:"from_price"`
	ToPrice   float32 `json:"to_price"`
	Price     float32 `json:"price"`
	CreatedAt string  `json:"created_at"`
	UpdatedAt string  `json:"updated_at"`
}

type UpdateAlternativeValue struct {
	ID        string  `json:"-"`
	FromPrice float32 `json:"from_price"`
	ToPrice   float32 `json:"to_price"`
	Price     float32 `json:"price"`
}

type AlternativeValueRequest struct {
	Page      int     `json:"page"`
	Limit     int     `json:"limit"`
	FromPrice float32 `json:"from_price"`
	ToPrice   float32 `json:"to_price"`
}

type AlternativeValueResponse struct {
	AlternativeValues []AlternativeValue `json:"alternative_values"`
	Count             int32              `json:"count"`
}
