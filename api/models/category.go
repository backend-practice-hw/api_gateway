package models

type Category struct {
	ID          string `json:"id"`
	Title       string `json:"title"`
	Image       string `json:"image"`
	Active      bool   `json:"active"`
	ParentID    string `json:"parent_id"`
	OrderNumber string `json:"order_number"`
	CreatedAt   string `json:"created_at"`
	UpdatedAt   string `json:"updated_at"`
}

type CreateCategory struct {
	Title    string `json:"title"`
	Image    string `json:"image"`
	Active   bool   `json:"active"`
	ParentID string `json:"parent_id"`
}

type UpdateCategory struct {
	ID       string `json:"-"`
	Title    string `json:"title"`
	Image    string `json:"image"`
	Active   bool   `json:"active"`
	ParentID string `json:"parent_id"`
}

type CategoryRequest struct {
	Page  int32  `json:"page"`
	Limit int32  `json:"limit"`
	Title string `json:"title"`
}

type CategoryResponse struct {
	Categories []Category `json:"categories"`
	Count      int32      `json:"count"`
}
