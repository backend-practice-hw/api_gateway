package models

type Courier struct {
	ID            string `json:"id"`
	FirstName     string `json:"first_name"`
	LastName      string `json:"last_name"`
	Phone         string `json:"phone"`
	Active        bool   `json:"active"`
	Login         string `json:"login"`
	Password      string `json:"password"`
	MaxOrderCount int32  `json:"max_order_count"`
	BranchID      string `json:"branch_id"`
	CreatedAt     string `json:"created_at"`
	UpdatedAt     string `json:"updated_at"`
}

type CreateCourier struct {
	FirstName     string `json:"first_name"`
	LastName      string `json:"last_name"`
	Phone         string `json:"phone"`
	Active        bool   `json:"active"`
	Login         string `json:"login"`
	Password      string `json:"password"`
	MaxOrderCount int32  `json:"max_order_count"`
	BranchID      string `json:"branch_id"`
}

type UpdateCourier struct {
	FirstName     string `json:"first_name"`
	LastName      string `json:"last_name"`
	Phone         string `json:"phone"`
	Active        bool   `json:"active"`
	Login         string `json:"login"`
	Password      string `json:"password"`
	MaxOrderCount int32  `json:"max_order_count"`
	BranchID      string `json:"branch_id"`
}

type CourierRequest struct {
	Page          int32  `json:"page"`
	Limit         int32  `json:"limit"`
	FirstName     string `json:"first_name"`
	LastName      string `json:"last_name"`
	Phone         string `json:"phone"`
	FromCreatedAt string `json:"from_created_at"`
	ToCreatedAt   string `json:"to_created_at"`
}

type CourierResponse struct {
	Couriers []Courier `json:"couriers"`
	Count    int32     `json:"count"`
}
