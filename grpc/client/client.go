package client

import (
	"api_gateway/config"
	pbC "api_gateway/genproto/catalog_service"
	pbO "api_gateway/genproto/order_service"
	pbU "api_gateway/genproto/user_service"
	"google.golang.org/grpc"
)

type IServiceManager interface {
	// catalog service
	CategoryService() pbC.CategoryServiceClient
	ProductService() pbC.ProductServiceClient

	// user service
	AdminService() pbU.AdminServiceClient
	CourierService() pbU.CourierServiceClient
	ClientService() pbU.ClientServiceClient
	BranchService() pbU.BranchServiceClient

	// order service
	OrderService() pbO.OrderServiceClient
	OrderProductService() pbO.OrderProductServiceClient
	DeliveryTariffService() pbO.DeliveryTariffServiceClient
	AlternativeValueService() pbO.AlternativeValueServiceClient
}

type grpcClient struct {
	// catalog service
	categoryService pbC.CategoryServiceClient
	productService  pbC.ProductServiceClient

	// user service
	adminService   pbU.AdminServiceClient
	courierService pbU.CourierServiceClient
	clientService  pbU.ClientServiceClient
	branchService  pbU.BranchServiceClient

	// order service
	orderService          pbO.OrderServiceClient
	orderProductService   pbO.OrderProductServiceClient
	deliveryTariffService pbO.DeliveryTariffServiceClient
	alternativeValue      pbO.AlternativeValueServiceClient
}

func NewGrpcClient(cfg config.Config) (IServiceManager, error) {
	connCatalogService, err := grpc.Dial(cfg.CatalogGrpcServiceHost+cfg.CatalogGrpcServicePort, grpc.WithInsecure())
	if err != nil {
		return nil, err
	}

	connUserService, err := grpc.Dial(cfg.UserGrpcServiceHost+cfg.UserGrpcServicePort, grpc.WithInsecure())
	if err != nil {
		return nil, err
	}

	connOrderService, err := grpc.Dial(cfg.OrderGrpcServiceHost+cfg.OrderGrpcServicePort, grpc.WithInsecure())
	if err != nil {
		return nil, err
	}

	return &grpcClient{
		// catalog service
		categoryService: pbC.NewCategoryServiceClient(connCatalogService),
		productService:  pbC.NewProductServiceClient(connCatalogService),

		// user service
		adminService:   pbU.NewAdminServiceClient(connUserService),
		courierService: pbU.NewCourierServiceClient(connUserService),
		clientService:  pbU.NewClientServiceClient(connUserService),
		branchService:  pbU.NewBranchServiceClient(connUserService),

		// order service
		orderService:          pbO.NewOrderServiceClient(connOrderService),
		orderProductService:   pbO.NewOrderProductServiceClient(connOrderService),
		deliveryTariffService: pbO.NewDeliveryTariffServiceClient(connOrderService),
		alternativeValue:      pbO.NewAlternativeValueServiceClient(connOrderService),
	}, nil
}

// catalog service
func (g *grpcClient) CategoryService() pbC.CategoryServiceClient {
	return g.categoryService
}

func (g *grpcClient) ProductService() pbC.ProductServiceClient {
	return g.productService
}

// user service
func (g *grpcClient) AdminService() pbU.AdminServiceClient {
	return g.adminService
}

func (g *grpcClient) CourierService() pbU.CourierServiceClient {
	return g.courierService
}

func (g *grpcClient) ClientService() pbU.ClientServiceClient {
	return g.clientService
}

func (g *grpcClient) BranchService() pbU.BranchServiceClient {
	return g.branchService
}

// order service
func (g *grpcClient) OrderService() pbO.OrderServiceClient {
	return g.orderService
}

func (g *grpcClient) OrderProductService() pbO.OrderProductServiceClient {
	return g.orderProductService
}

func (g *grpcClient) DeliveryTariffService() pbO.DeliveryTariffServiceClient {
	return g.deliveryTariffService
}

func (g *grpcClient) AlternativeValueService() pbO.AlternativeValueServiceClient {
	return g.alternativeValue
}
